
# Keyness project (Trier & al)

**See**

* [Workshop: Using and Developing Software for Keyness Analysis](https://zeta-project.eu/en/workshops/)
* [Zeta and Company](https://zeta-project.eu/en/)
  * [pydistinto ](https://github.com/Zeta-and-Company/pydistinto)
* [Textometry project](https://txm.gitpages.huma-num.fr/textometrie/en/)

## Trier workflow terminology

**Source**

Du, Keli, Dudar, Julia, Rok, Cora, & Schöch, Christof. (2021). Implementation framework of measures of distinctiveness. Zenodo. https://doi.org/10.5281/zenodo.5092328

### 1. Pre-processing

```mermaid

%%{ init: { 'flowchart': { 'curve': 'monotoneY' } } }%%

flowchart LR

  classDef Box fill:none,stroke:CornflowerBlue ,stroke-width:1px;
  classDef default fill:none,stroke:none,stroke-width:4px;
  
  InputData[input data] ==> Xml[.xml]
  Xml =="<p style='background-color: white; padding: 0.3em;'>remove xml-Tags,<br/>teiHeaders etc.</p>"==> Txt[.txt]
  InputData ==> Txt
  Txt ==> Nlp

  subgraph Nlp[" "]
    direction LR
    Lemmatization[lemmatization] ==> Lemma[lemma]
    POS-Tagging ==> POS-tags
    Empty[" "] ==> Tokens[tokens]
  end

  Nlp ==> N-grams
  N-grams ==> PreparedTexts["prepared texts for analysis"]
  Nlp ==> PreparedTexts

class Nlp Box;
linkStyle 0,1,2,3,4,5,6,7,8,9 stroke-width:4px,fill:none,stroke:gold;
linkStyle 4,5,6 stroke-width:4px,fill:none,stroke:black;

```

### 2. Building groups

```mermaid

%%{ init: { 'flowchart': { 'curve': 'monotoneY' } } }%%

flowchart LR

  classDef default fill:none,stroke:none,stroke-width:4px;
  
  PreparedTexts["prepared texts for analysis"] =="<p style='background-color: white; padding: 0.3em;'>splitting<br/>len(segment)</p>"==> Segments[segments]
  Segments =="<p style='background-color: white; padding: 0.3em;'>convert the segments<br/>to a doc-term-matrix</p>"==> MetaDocTerm["<p style='text-align: left;'>metadata.csv<br/>& doc-term-matrix for target group<br/>& doc-term-matrix for comparison group</p>"]

linkStyle 0,1 stroke-width:4px,fill:none,stroke:gold;

```

### 3. Counting & Calculating

```mermaid

%%{ init: { 'flowchart': { 'curve': 'monotoneY' } } }%%

flowchart LR

  classDef Box fill:none,stroke:CornflowerBlue,stroke-width:1px;
  classDef default fill:none,stroke:none,stroke-width:4px;
  
  MetaDocTerm["<p style='text-align: left;'>metadata.csv<br/>& doc-term-matrix for target group<br/>& doc-term-matrix for comparison group</p>"]=="<p style='background-color: white; padding: 0.3em;'>counting</p>"==> Numbers

  subgraph Numbers["numbers"]
    direction LR
    LeftNum["<ul style='text-align: left;'><li>term_freq(w) in segment s</li><li>no. of w in s</li><li>index(w) in s</li><li>segment_freq(w)</li><li>...</li></ul>"] =="<p style='background-color: white; padding: 0.3em;'>calculating</p>"==> RightNum["<ul style='text-align: left;'><li>relative_term_freq(w)</li><li>relative_segment_freq(w)</li><li>mean(w)</li><li>median(w)</li><li>variance(w)</li><li>std(w)...</li><li>segment_ranking based on term_freq(w)</li><li>inter-arrival-times(w)</li><li>...</li></ul>"]
  end

  Numbers =="<p style='background-color: white; padding: 0.3em;'>save data</p>"==> Dataframe["<p style='color:#70AE42;'>dataframe.csv</p>"]

class Numbers Box;
linkStyle 0,1 stroke-width:6px,fill:none,stroke:gold
linkStyle 2 stroke-width:6px,fill:none,stroke:#70AE42

```

### 4. Measures of distinctiveness

```mermaid

%%{ init: { 'flowchart': { 'curve': 'monotoneY' } } }%%

flowchart LR

  classDef default fill:none,stroke:none,stroke-width:4px;
  
  Numbers["numbers"] ==> Measures

  subgraph Measures[" "]
    direction LR
    MeasuresList["<ul style='text-align: left;'><li>𝜒<sup>2</sub></li><li>Log-likelihood ratio</li><li>Welch's t</li><li>inter-arrival-time</li><li>Wilcoxon Rank-sum</li><li>Zeta</li><li>tf-idf weighting</li><li>...</li></ul>"] ==> Vis["Visualization of most<br/>distinctive words<br/><ul style='text-align: left;'><li>Bar chart</li><li>Scatterplot</li><li>...</li></ul>"]
    Vis =="<p style='background-color: white; padding: 0.3em;'>save data</p>"==> Dataplot["<p style='color:#70AE42;'>plots.svg / plots.jpg</p>"]
    MeasuresList ==> Test["test statistic values"]
  end

linkStyle 0,1,3 stroke-width:4px,fill:none,stroke:gold
linkStyle 2 stroke-width:4px,fill:none,stroke:#70AE42

```
